<!--
This file is part of the SV-Benchmarks collection of verification tasks:
https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks

SPDX-FileCopyrightText: 2022 Dirk Beyer

SPDX-License-Identifier: Apache-2.0
-->

## Word-Level Hardware-Model-Checking Benchmarks with Arrays and Bit-Vectors

These programs were produced by [Btor2C](sosy-lab/software/btor2c@a0fa249), a word-level-circuit-to-C converter, from word-level hardware-model-checking tasks described in the Btor2 format.

The original hardware tasks consist of both array and bit-vector types.

Each converted C program contains a URL to its source and is under the same license as its original Btor2 file.

The benchmark set was prepared by Po-Chun Chien and Nian-Ze Lee from SoSy-Lab, LMU Munich.
